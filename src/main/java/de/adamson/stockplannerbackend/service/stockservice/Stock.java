package de.adamson.stockplannerbackend.service.stockservice;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Stock {

    private String isin;
    private String name;
    private String currency;
    private Double price;
    private String capitalisation;
    private Integer morningStarRating;
    private Performance performance;
    private Performance performanceRelativeToSector;

    @Builder
    @Getter
    public static class Performance {

        private Double perf1month;
        private Double perf6month;
        private Double perf1year;
        private Double perf3year;
        private Double perf5year;
        private Double perf10year;

    }

}
