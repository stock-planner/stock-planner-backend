package de.adamson.stockplannerbackend.service.stockservice;

import de.adamson.stockplannerbackend.service.financeinfo.CachedFinanceInfoService;
import de.adamson.stockplannerbackend.service.financeinfo.FinanceInfoSectors;
import de.adamson.stockplannerbackend.service.financeinfo.FinanceInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class StockService {

    private final FinanceInfoService financeInfoService;

    @Autowired
    public StockService(CachedFinanceInfoService financeInfoService) {
        this.financeInfoService = financeInfoService;
    }

    public Flux<Sector> getSectors() {
        return this.financeInfoService.getSectors()
                .flatMapIterable(FinanceInfoSectors::getSectors)
                .flatMap(row -> Mono.zip(
                        Mono.just(row),
                        this.getStocksBySector(row.getKey()).collectList(),
                        this.getAvgPerformanceBySector(row.getKey()),
                        this.getMaxPerformanceBySector(row.getKey())
                ))
                .map(objects -> Sector.builder()
                        .id(objects.getT1().getKey())
                        .name(objects.getT1().getValue())
                        .stocks(objects.getT2())
                        .avgPerformance(objects.getT3())
                        .maxPerformance(objects.getT4())
                        .build()
                );
    }

    private Mono<Sector.Performance> getAvgPerformanceBySector(int sectorId) {
        return this.financeInfoService.getStocksBySector(sectorId)
                .last()
                .map(stock -> Sector.Performance.builder()
                        .perf1month(stock.getQuartil().getPerf1month().getSectorAvgPerf())
                        .perf6month(stock.getQuartil().getPerf6month().getSectorAvgPerf())
                        .perf1year(stock.getQuartil().getPerf1year().getSectorAvgPerf())
                        .perf3year(stock.getQuartil().getPerf3year().getSectorAvgPerf())
                        .perf5year(stock.getQuartil().getPerf5year().getSectorAvgPerf())
                        .perf10year(stock.getQuartil().getPerf10year().getSectorAvgPerf())
                        .build());
    }

    private Mono<Sector.Performance> getMaxPerformanceBySector(int sectorId) {
        return this.financeInfoService.getStocksBySector(sectorId)
                .last()
                .map(stock -> Sector.Performance.builder()
                        .perf1month(stock.getQuartil().getPerf1month().getSectorMaxPerf())
                        .perf6month(stock.getQuartil().getPerf6month().getSectorMaxPerf())
                        .perf1year(stock.getQuartil().getPerf1year().getSectorMaxPerf())
                        .perf3year(stock.getQuartil().getPerf3year().getSectorMaxPerf())
                        .perf5year(stock.getQuartil().getPerf5year().getSectorMaxPerf())
                        .perf10year(stock.getQuartil().getPerf10year().getSectorMaxPerf())
                        .build());
    }

    public Flux<Stock> getStocksBySector(int sectorId) {
        return this.financeInfoService.getStocksBySector(sectorId)
                .map(stock -> Stock.builder()
                        .isin(stock.getInfo().getIsin())
                        .name(stock.getBasic().getName())
                        .currency(stock.getPrice().getCurrency())
                        .price(stock.getPrice().getPrice())
                        .capitalisation(stock.getCharacteristics().getCapitalisation())
                        .morningStarRating(stock.getMorningStar().getRating())
                        .performance(Stock.Performance.builder()
                                .perf1month(stock.getQuartil().getPerf1month().getPerf())
                                .perf6month(stock.getQuartil().getPerf6month().getPerf())
                                .perf1year(stock.getQuartil().getPerf1year().getPerf())
                                .perf3year(stock.getQuartil().getPerf3year().getPerf())
                                .perf5year(stock.getQuartil().getPerf5year().getPerf())
                                .perf10year(stock.getQuartil().getPerf10year().getPerf())
                                .build())
                        .performanceRelativeToSector(Stock.Performance.builder()
                                .perf1month(stock.getQuartil().getPerf1month().getPerfRelativeToSector())
                                .perf6month(stock.getQuartil().getPerf6month().getPerfRelativeToSector())
                                .perf1year(stock.getQuartil().getPerf1year().getPerfRelativeToSector())
                                .perf3year(stock.getQuartil().getPerf3year().getPerfRelativeToSector())
                                .perf5year(stock.getQuartil().getPerf5year().getPerfRelativeToSector())
                                .perf10year(stock.getQuartil().getPerf10year().getPerfRelativeToSector())
                                .build())
                        .build()
                );
    }

}
