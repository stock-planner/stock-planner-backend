package de.adamson.stockplannerbackend.service.stockservice;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class Sector {

    private Integer id;
    private String name;
    private Performance avgPerformance;
    private Performance maxPerformance;
    private final List<Stock> stocks;

    @Builder
    @Getter
    public static class Performance {

        private Double perf1month;
        private Double perf6month;
        private Double perf1year;
        private Double perf3year;
        private Double perf5year;
        private Double perf10year;

    }

}
