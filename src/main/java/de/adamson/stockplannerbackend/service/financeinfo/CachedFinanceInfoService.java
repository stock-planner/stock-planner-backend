package de.adamson.stockplannerbackend.service.financeinfo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class CachedFinanceInfoService extends FinanceInfoService {

    private Mono<FinanceInfoSectors> sectorsCache;
    private Map<Integer, Flux<FinanceInfoStock>> stocksBySectorCache;

    public CachedFinanceInfoService() {
        this.stocksBySectorCache = new HashMap<>();
    }

    @Override
    public Mono<FinanceInfoSectors> getSectors() {
        if(this.sectorsCache == null) {
            this.sectorsCache = super.getSectors().retry().cache(
                    sectors -> Duration.ofHours(12),
                    throwable -> Duration.ZERO,
                    () -> Duration.ZERO
            );
        }
        return this.sectorsCache;
    }

    @Override
    public Flux<FinanceInfoStock> getStocksBySector(int sectorId) {
        if(this.stocksBySectorCache.get(sectorId) == null) {
            this.stocksBySectorCache.put(sectorId, super.getStocksBySector(sectorId).collectList().retry().cache(
                    stocks -> Duration.ofHours(12),
                    throwable -> Duration.ZERO,
                    () -> Duration.ZERO
            ).flatMapIterable(stocks -> stocks));
        }
        return this.stocksBySectorCache.get(sectorId);
    }

    @Scheduled(fixedRate = 4 * 60 * 60 * 1000) // Executed every 4 hours
    public void refreshCaches() {
        log.info("polling caches...");
        this.getSectors().subscribe(sectors -> sectors.getSectors().forEach(sector -> this.getStocksBySector(sector.getKey()).subscribe()));
    }

}
