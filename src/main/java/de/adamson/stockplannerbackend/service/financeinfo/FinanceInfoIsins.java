package de.adamson.stockplannerbackend.service.financeinfo;

import lombok.Getter;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@XmlRootElement(name = "REPLY")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
public class FinanceInfoIsins {

    @XmlElement(name = "BLOCK")
    private Block block;

    public List<String> getIsins() {
        try {
            return this.block.getRowSet().getRows().stream().map(Block.RowSet.Row::getIsin).collect(Collectors.toList());
        } catch (NullPointerException e) {
            return new ArrayList<>();
        }
    }

    public int getAmountTotal() {
        return this.block.amountTotal;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @Getter
    public static class Block {

        @XmlElement(name = "AMOUNT_TOTAL")
        private int amountTotal;

        @XmlElement(name = "ROWSET")
        private RowSet rowSet;

        @XmlAccessorType(XmlAccessType.FIELD)
        @Getter
        public static class RowSet {

            @XmlElement(name = "ROW")
            private List<Row> rows;

            @XmlAccessorType(XmlAccessType.FIELD)
            @Getter
            public static class Row {

                @XmlElement(name = "ISIN")
                private String isin;

            }

        }

    }

}
