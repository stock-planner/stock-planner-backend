package de.adamson.stockplannerbackend.service.financeinfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class FinanceInfoStock {

    @JsonProperty("Info")
    private Info info;

    @JsonProperty("BasicV1")
    private Basic basic;

    @JsonProperty("CharacteristicsV1")
    private Characteristics characteristics;

    @JsonProperty("PriceV2")
    private Price price;

    @JsonProperty("MorningStarV1")
    private MorningStar morningStar;

    @JsonProperty("QuartilV1")
    private Quartil quartil;

    @Getter
    public static class Info {

        @JsonProperty("ID")
        private String isin;

    }

    @Getter
    public static class Basic {

        @JsonProperty("NAME_SECURITY")
        private String name;

    }

    @Getter
    public static class Characteristics {

        @JsonProperty("NAME_TYPE_CAPITALISATION")
        private String capitalisation;

    }

    @Getter
    public static class Price {

        @JsonProperty("ISO_CURRENCY")
        private String currency;

        @JsonProperty("PRICE")
        private Double price;

    }

    @Getter
    public static class MorningStar {

        @JsonProperty("RATING")
        private Integer rating;

    }

    @Getter
    public static class Quartil {

        @JsonProperty("1M")
        private Performance perf1month;

        @JsonProperty("6M")
        private Performance perf6month;

        @JsonProperty("1Y")
        private Performance perf1year;

        @JsonProperty("3Y")
        private Performance perf3year;

        @JsonProperty("5Y")
        private Performance perf5year;

        @JsonProperty("10Y")
        private Performance perf10year;

        @Getter
        public static class Performance {

            @JsonProperty("PERFORMANCE_TIME_SPAN_PCT")
            private Double perf;

            @JsonProperty("PERFORMANCE_RELATIVE_PCT")
            private Double perfRelativeToSector;

            @JsonProperty("SECTOR_AVG_PERFORMANCE_PCT")
            private Double sectorAvgPerf;

            @JsonProperty("SECTOR_BEST_PERFORMANCE_PCT")
            private Double sectorMaxPerf;

        }

    }

}
