package de.adamson.stockplannerbackend.service.financeinfo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.resources.ConnectionProvider;

import java.util.concurrent.atomic.AtomicInteger;

@Service
@Slf4j
public class FinanceInfoService {

    private WebClient webClient;

    public FinanceInfoService() {
        HttpClient httpClient = HttpClient.create(ConnectionProvider.builder("webclient-fixed").maxConnections(10).pendingAcquireMaxCount(100000).build());
        this.webClient = WebClient.builder()
                .baseUrl("https://www.consorsbank.de")
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .build();
    }

    public Mono<FinanceInfoSectors> getSectors() {
        return this.webClient
                .get()
                .uri("/euroWebDe/servlets/financeinfos_ajax?page=funds.maincategories&parentCategory=39751")
                .retrieve()
                .bodyToMono(FinanceInfoSectors.class)
                .doFinally(s -> log.info("requested all sectors"));
    }

    public Flux<FinanceInfoStock> getStocksBySector(int sectorId) {
        return this.getIsinsBySector(sectorId).flatMap(this::getStockByIsin);
    }

    private Flux<String> getIsinsBySector(int sectorId) {
        AtomicInteger offset = new AtomicInteger();
        return this.getIsinsBySector(sectorId, offset.getAndIncrement())
                .expand(financeInfoIsins -> {
                    int currentOffset = offset.getAndIncrement();
                    if((financeInfoIsins.getAmountTotal() / 15) + 1 == currentOffset) {
                        offset.set(1);
                        return Flux.empty();
                    }
                    return this.getIsinsBySector(sectorId, currentOffset);
                }, 15)
                .flatMapIterable(FinanceInfoIsins::getIsins)
                .doFinally(s -> log.info("requested isins of sector with id " + sectorId));
    }

    private Mono<FinanceInfoIsins> getIsinsBySector(int sectorId, int offset) {
        return this.webClient
                .get()
                .uri("/euroWebDe/servlets/financeinfos_ajax?page=funds.finder&category=" + sectorId + "&pageoffset=" + offset + "&layout=ISIN")
                .retrieve()
                .bodyToMono(FinanceInfoIsins.class);
    }

    private Mono<FinanceInfoStock> getStockByIsin(String isin) {
        return this.webClient
                .get()
                .uri("/web-financialinfo-service/api/marketdata/funds?id=" + isin + "&field=BasicV1&field=CharacteristicsV1&field=PriceV2&field=MorningStarV1&field=QuartilV1")
                .retrieve()
                .bodyToFlux(FinanceInfoStock.class)
                .last()
                .doFinally(s -> log.info("requested stock details for isin " + isin));
    }

}
