package de.adamson.stockplannerbackend.service.financeinfo;

import lombok.Getter;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "REPLY")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
public class FinanceInfoSectors {

    @XmlElement(name = "BLOCK")
    private Block block;

    public List<Block.RowSet.Row> getSectors() {
        return this.getBlock().getRowSet().getRows();
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @Getter
    public static class Block {

        @XmlElement(name = "ROWSET")
        private RowSet rowSet;

        @XmlAccessorType(XmlAccessType.FIELD)
        @Getter
        public static class RowSet {

            @XmlElement(name = "ROW")
            private List<Row> rows;

            @XmlAccessorType(XmlAccessType.FIELD)
            @Getter
            public static class Row {

                @XmlElement(name = "KEY")
                private int key;
                @XmlElement(name = "VALUE")
                private String value;

            }

        }

    }

}
