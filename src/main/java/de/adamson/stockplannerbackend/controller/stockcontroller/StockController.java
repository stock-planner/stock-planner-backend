package de.adamson.stockplannerbackend.controller.stockcontroller;

import de.adamson.stockplannerbackend.service.stockservice.Stock;
import de.adamson.stockplannerbackend.service.stockservice.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@CrossOrigin
@RestController
public class StockController {

    private StockService stockService;

    @Autowired
    public StockController(StockService stockService) {
        this.stockService = stockService;
    }

    @GetMapping(value = "/sectors")
    public Flux<SectorAggregatedDto> getSectors() {
        return this.stockService.getSectors().map(sector -> SectorAggregatedDto.builder()
                .id(sector.getId())
                .name(sector.getName())
                .avgPerformance(SectorAggregatedDto.SectorPerformanceDto.builder()
                        .perf1month(sector.getAvgPerformance().getPerf1month())
                        .perf6month(sector.getAvgPerformance().getPerf6month())
                        .perf1year(sector.getAvgPerformance().getPerf1year())
                        .perf3year(sector.getAvgPerformance().getPerf3year())
                        .perf5year(sector.getAvgPerformance().getPerf5year())
                        .perf10year(sector.getAvgPerformance().getPerf10year())
                        .build())
                .maxPerformance(SectorAggregatedDto.SectorPerformanceDto.builder()
                        .perf1month(sector.getMaxPerformance().getPerf1month())
                        .perf6month(sector.getMaxPerformance().getPerf6month())
                        .perf1year(sector.getMaxPerformance().getPerf1year())
                        .perf3year(sector.getMaxPerformance().getPerf3year())
                        .perf5year(sector.getMaxPerformance().getPerf5year())
                        .perf10year(sector.getMaxPerformance().getPerf10year())
                        .build())
                .build()
        );
    }

    @GetMapping(value = "/sectors/{id}/stocks")
    public Flux<Stock> getStocksBySectorId(@PathVariable("id") Integer sectorId) {
        return this.stockService.getStocksBySector(sectorId);
    }

}
