package de.adamson.stockplannerbackend.controller.stockcontroller;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SectorAggregatedDto {

    private Integer id;
    private String name;
    private SectorPerformanceDto avgPerformance;
    private SectorPerformanceDto maxPerformance;

    @Getter
    @Builder
    public static class SectorPerformanceDto {

        private Double perf1month;
        private Double perf6month;
        private Double perf1year;
        private Double perf3year;
        private Double perf5year;
        private Double perf10year;

    }

}
