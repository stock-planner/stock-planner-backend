FROM openjdk:11-jre

WORKDIR /app
COPY target/*.jar app.jar

EXPOSE 80/tcp
ENTRYPOINT ["java", "-jar", "app.jar"]
